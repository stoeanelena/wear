package ro.emag.bewear;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class AsyncT extends AsyncTask<String, String, String> {

    private final String USER_AGENT = "Mozilla/5.0";
    private final String URL_WARNING_RECEIVE = "http://104.248.45.34/warning/receive";
    private final String HTTP_METHOD_POST = "POST";

    @Override
    protected String doInBackground(String... params) {
        try {
            String url = URL_WARNING_RECEIVE;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod(HTTP_METHOD_POST);
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-type", "application/json");

            GpsCoordsData gpsCoords = new GpsCoordsData();
            gpsCoords.setLatitude(params[0]);
            gpsCoords.setLongitude(params[1]);
            gpsCoords.setUserId(1);
            gpsCoords.setStatus("danger");

            Gson gson = new Gson();
            String gpsJson = gson.toJson(gpsCoords);

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(gpsJson);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("Post parameters : :" + gpsJson);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}