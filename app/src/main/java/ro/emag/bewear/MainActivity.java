package ro.emag.bewear;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class MainActivity extends WearableActivity {

    private TextView mTextView;
    private FusedLocationProviderClient mFusedLocationClient;

    private static String latitude;
    private static String longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.text);

        final Button dangerButton = findViewById(R.id.danger_button);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 10);
            Log.e("ERROR:", "No permission to LOCATION !!");
        }

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Double latitudeLong = location.getLatitude();
                        Double longitudeLong = location.getLongitude();

                        String lat = latitudeLong.toString();
                        String lng = longitudeLong.toString();

                        Log.i("Latitude: ", "Value: " + latitudeLong);
                        Log.i("Longitude: ", "Value: " + longitudeLong);

                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            latitude = lat;
                            longitude = lng;
                        }
                    }
                });

        dangerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall(latitude, longitude);
                dialContactPhone("0784140148");
            }
        });

        // Enables Always-on
        setAmbientEnabled();
    }

    private void dialContactPhone(final String phoneNumber) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.CALL_PHONE}, 10);
            Log.e("ERROR:", "No permission to CALL !!");
        }
        startActivity(new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phoneNumber, null)));
    }

    private void makeCall(String latitude, String longitude) {
        final AsyncT asyncT = new AsyncT();
        asyncT.execute(latitude, longitude);
    }
}
